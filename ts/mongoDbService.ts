class MongoDbService {
    private collection: string;

    constructor(private url: string, private mongoClient: any) { }

    connect(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) { reject(err); }
                resolve(db);
            });
        })
    }

    createCollection(name: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.createCollection(name, (err, res) => {
                        if (err) { reject(err); }
                        resolve(res);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            }).catch(err => reject(err));
        });
    }

    setCollection(collection: string) {
        if (!collection) throw Error('collection not set');
        this.collection = collection;
        return this;
    }

    getCollection() {
        return this.collection;
    }

    insert(entry: object): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).insertOne(entry, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(res);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error + this.collection);
                }
            }).catch(err => reject(err));
        });
    }

    insertMany(entries: Array<object>) {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).insertMany(entries, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(res);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    findOne(query: object = {}): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).findOne(query, (err, result) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    find(query: object = {}): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).find(query).toArray((err, result) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    deleteOne(query: object = {}): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).deleteOne(query).toArray((err, result) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    deleteMany(query: object = {}): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).deleteMany(query).toArray((err, result) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    dropCollection() {
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).drop((err, delOk) => {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    updateOne(query:string,newObject:object):Promise<any>{
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).updateOne(query,newObject,(err, delOk) => {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }

    updateMany(query:string,newFieldObject:object):Promise<any>{
        return new Promise((resolve, reject) => {
            this.connect().then(db => {
                try {
                    db.collection(this.collection).updateMany(query,newFieldObject,(err, delOk) => {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                } catch (error) {
                    reject(error);
                }
            })
                .catch(err => reject(err));
        });
    }



}

module.exports = MongoDbService;