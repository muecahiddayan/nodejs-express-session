class SessionService {
    constructor(private mongoDbService: MongoDbService,private session) { }

    login() {

    }

    checkForLogin( email, password ):Promise<any> {
        return new Promise((resolve, reject) => {
            this.mongoDbService.findOne({ email }).then(user => {
                if(!user){
                    reject({status:false,message:'User does not exist'});
                }
                let { userPassword } = user;
                if(userPassword === password){
                    resolve(true);
                }else{
                    reject({status:false,message:'Wrong password'});
                }
            }).catch(message => reject({status:false,message}));
        });
    }

    logIn(email,password):void{
        this.checkForLogin(email,password).then(status=>{
            if(status){
                
            }
        }).catch(error=>error);
    }

    isLoggedIn():boolean {
        return false;
    }
}

module.exports = Session;