const express = require('express');
const router = express.Router();

const services = require('../services/');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index',{title:'Login',loggedIn:services.Session.isLoggedIn()});
  /*
  services.MongoDbService.insert({email:"m.dayan@pmedia.de",password:'12345678'}).then(user => {
    res.send(user.name);    
  }).catch(err => res.send(err));
  services.MongoDbService.deleteMany({name:/.*ayan/}).then(user => {
    res.send(user.name);    
  }).catch(err => res.send(err));
  */
  // res.render('index', { title: 'Login' });
});

module.exports = router;
