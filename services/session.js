var SessionService = /** @class */ (function () {
    function SessionService(mongoDbService, session) {
        this.mongoDbService = mongoDbService;
        this.session = session;
    }
    SessionService.prototype.login = function () {
    };
    SessionService.prototype.checkForLogin = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.mongoDbService.findOne({ email: email }).then(function (user) {
                if (!user) {
                    reject({ status: false, message: 'User does not exist' });
                }
                var userPassword = user.userPassword;
                if (userPassword === password) {
                    resolve(true);
                }
                else {
                    reject({ status: false, message: 'Wrong password' });
                }
            })["catch"](function (message) { return reject({ status: false, message: message }); });
        });
    };
    SessionService.prototype.logIn = function (email, password) {
        this.checkForLogin(email, password).then(function (status) {
            if (status) {
            }
        })["catch"](function (error) { return error; });
    };
    SessionService.prototype.isLoggedIn = function () {
        return false;
    };
    return SessionService;
}());
module.exports = Session;
