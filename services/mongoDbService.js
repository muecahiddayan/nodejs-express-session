var MongoDbService = /** @class */ (function () {
    function MongoDbService(url, mongoClient) {
        this.url = url;
        this.mongoClient = mongoClient;
    }
    MongoDbService.prototype.connect = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.mongoClient.connect(_this.url, function (err, db) {
                if (err) {
                    reject(err);
                }
                resolve(db);
            });
        });
    };
    MongoDbService.prototype.createCollection = function (name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.createCollection(name, function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        resolve(res);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.setCollection = function (collection) {
        if (!collection)
            throw Error('collection not set');
        this.collection = collection;
        return this;
    };
    MongoDbService.prototype.getCollection = function () {
        return this.collection;
    };
    MongoDbService.prototype.insert = function (entry) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).insertOne(entry, function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        resolve(res);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error + _this.collection);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.insertMany = function (entries) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).insertMany(entries, function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        resolve(res);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.findOne = function (query) {
        var _this = this;
        if (query === void 0) { query = {}; }
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).findOne(query, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.find = function (query) {
        var _this = this;
        if (query === void 0) { query = {}; }
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).find(query).toArray(function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.deleteOne = function (query) {
        var _this = this;
        if (query === void 0) { query = {}; }
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).deleteOne(query).toArray(function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.deleteMany = function (query) {
        var _this = this;
        if (query === void 0) { query = {}; }
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).deleteMany(query).toArray(function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.dropCollection = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).drop(function (err, delOk) {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.updateOne = function (query, newObject) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).updateOne(query, newObject, function (err, delOk) {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    MongoDbService.prototype.updateMany = function (query, newFieldObject) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connect().then(function (db) {
                try {
                    db.collection(_this.collection).updateMany(query, newFieldObject, function (err, delOk) {
                        if (err) {
                            reject(err);
                        }
                        if (delOk) {
                            resolve(delOk);
                        }
                        db.close();
                    });
                }
                catch (error) {
                    reject(error);
                }
            })["catch"](function (err) { return reject(err); });
        });
    };
    return MongoDbService;
}());
module.exports = MongoDbService;
