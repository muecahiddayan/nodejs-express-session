const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/mydb";
const session = require('express-session');
const MongoDbService = new (require('../services/mongoDbService'))(url, MongoClient).setCollection('users');
const SessionService = new (require('../services/session'))(MongoDbService,session);

module.exports =  Services = {
    MongoDbService,
    SessionService,
}
